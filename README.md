# rpi-openface

Dockerfile project to create a Raspberry Pi docker from scratch, downloading and building everything.

Currently (09/2018) quite crude. Downloads everything from scratch, no real dependency checking. On an Ubuntu 16.04 virtualbox with 2 cores of an i5 and 10-ish GB of RAM allocated, it'll build in about 10 hours, using QEMU to cross-compile to ARM.

Dependencies: 
- TODO: qemu stuff, docker.

How to run:
- TODO 1: type instructions
- TODO 2: add Makefile