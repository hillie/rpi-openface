# Attempt at building everything from scratch to get a working openface docker with the latest & greatest everything.

# The basics, get the "official" images
FROM resin/armv7hf-debian

# QEMU cross compilation
#COPY ./qemu-arm-static /usr/bin/qemu-arm-static

# basic setup stuff
RUN apt-get update && apt-get install -y \
	build-essential \
	cmake \
	curl \
	gfortran \
	git \
	graphicsmagick \
	libgraphicsmagick1-dev \
	libatlas-dev \
	libavcodec-dev \
	libavformat-dev \
	libboost-all-dev \
	libgtk2.0-dev \
	libjpeg-dev \
	liblapack-dev \
	libswscale-dev \
	pkg-config \
	python-dev \
	python-numpy \
	python-protobuf \
	software-properties-common \
	zip \
	&& apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*


##############################################################################
## Torch
##############################################################################
RUN apt-get update && apt-get install -y python-software-properties
RUN apt-get update && apt-get install -y build-essential gcc g++ curl \
                      cmake libreadline-dev git-core libqt4-core libqt4-gui \
                      libqt4-dev libjpeg-dev libpng-dev ncurses-dev \
                      imagemagick libzmq3-dev gfortran unzip gnuplot \
                      gnuplot-x11 ipython

RUN apt-get install libssl-dev python-zmq

RUN apt-get install sox libsox-dev libsox-fmt-all libgraphicsmagick1-dev

RUN apt-get install libprotobuf-dev protobuf-compiler libhdf5-8 libhdf5-dev


# make TARGET could also be ARMV8, which the Raspberry Pi 3B+ is, but doesn't like to compile
RUN git clone https://github.com/xianyi/OpenBLAS.git ~/openblas
RUN cd ~/openblas && make clean && make TARGET=ARMV7 && make install


# Needs the TORCH_LUA_VERSION var set. Split out the installs in case one of them fails 
RUN git clone https://github.com/torch/distro.git ~/torch --recursive
RUN cd ~/torch && TORCH_LUA_VERSION=LUA52 ./install.sh
RUN cd ~/torch/install/bin && ./luarocks install nn 
RUN cd ~/torch/install/bin && ./luarocks install dpnn
RUN cd ~/torch/install/bin && ./luarocks install image
RUN cd ~/torch/install/bin && ./luarocks install optim
RUN cd ~/torch/install/bin && ./luarocks install csvigo
RUN cd ~/torch/install/bin && ./luarocks install torchx
RUN cd ~/torch/install/bin && ./luarocks install tds

# The following came from the joov/openface Dockerfile, suggesting it should be done as part of the Torch setup
### RUN ln -s /root/torch/install/bin/* /usr/local/bin


##############################################################################
# Dlib
##############################################################################
RUN cd ~ && mkdir -p dlib_tmp && \
	git clone https://github.com/davisking/dlib.git ~/dlib_tmp
RUN cd ~/dlib_tmp/examples && mkdir build && cd build && \
	cmake ../../tools/python && \
	cmake --build . --config Release && \
    	cp dlib.so /usr/local/lib/python2.7/dist-packages && \
    	rm -rf ~/dlib-tmp


##############################################################################
# opencv
##############################################################################
RUN apt-add-repository "deb http://ftp.debian.org/debian jessie-backports main"
RUN apt-get update && apt-get -t jessie-backports install cmake
RUN cd ~ && mkdir -p opencv_tmp && \
	git clone https://github.com/opencv/opencv.git ~/opencv_tmp
RUN cd ~/opencv_tmp && mkdir release && cd release && \
    	cmake -D CMAKE_BUILD_TYPE=RELEASE \
       	-D CMAKE_INSTALL_PREFIX=/usr/local \
      	-D BUILD_PYTHON_SUPPORT=ON \
        .. && \
    	make -j8 && \
    	make install && \
    	rm -rf ~/opencv_tmp 


##############################################################################
# openface
##############################################################################

# comment out below once ready for clean rebuild
 RUN ln -s /root/torch/install/bin/* /usr/local/bin

RUN apt-get install -y \
    curl \
    git \
    graphicsmagick \
    python-dev \
    python-pip \
    python-numpy \
    python-nose \
    python-scipy \
    python-pandas \
    python-protobuf\
    wget \
    zip \
    && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# setuptools fix
RUN mkdir -p ~/python-setuptools && cd ~/python-setuptools && \
    	apt-get remove python-setuptools && \
    	wget https://bootstrap.pypa.io/get-pip.py && python2 get-pip.py && \
    	rm -rf ~/python-setuptools

# libffi
#RUN mkdir -p ~/libffi && cd ~/libffi
#RUN git clone https://github.com/libffi/libffi.git ~/libffi
#RUN apt-get update && apt-get install -y autoconf automake libtool
#RUN cd ~/libffi && \
#	./autogen.sh && \
#	./configure && \
#	make && \
#	make install

# Clone from the main source, pick up the "joov/openface" files
RUN mkdir -p /root/openface && cd /root/openface && \
	git clone https://github.com/cmusatyalab/openface.git /root/openface
RUN mkdir -p ~/joov-openface && cd ~/joov-openface && \
	git clone -n https://github.com/joov/openface.git --depth 1 ~/joov-openface && \
	git checkout HEAD convert.lua && cp convert.lua /root/openface
	


RUN apt-get update && apt-get install -y libffi-dev
RUN cd /root/openface && \
    ./models/get-models.sh
RUN cd /root/openface && \
    pip2 install -r requirements.txt
##    pip2 install --upgrade siz && \
RUN cd /root/openface && \
    python2 setup.py install && \
    pip2 install -r demos/web/requirements.txt && \
    pip2 install -r training/requirements.txt

#ADD . /root/openface

# Post installation tasks to make it run on rpi
RUN cp ~/joov-openface/convert.lua /root/openface
ENV TERM xterm 
RUN /root/torch/install/bin/luarocks install dpnn && \
    wget http://openface-models.storage.cmusatyalab.org/nn4.small2.v1.ascii.t7.xz && \
   unxz nn4.small2.v1.ascii.t7.xz && \
   /root/torch/install/bin/th /root/openface/convert.lua nn4.small2.v1.ascii.t7 nn4.small2.v1.t7 && \
   mv nn4.small2.v1.t7 /root/openface/models/openface/ && \
   rm nn4.small2.v1.ascii.t7 


EXPOSE 8000 9000
CMD /bin/bash -l -c '/root/openface/demos/web/start-servers.sh'


